package com.demo.testadaptatechnologies;

import com.demo.entity.Usuarios;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Gerardo Larín
 */
@ManagedBean(name = "sessionBean")
@RequestScoped
public class SessionBean implements Serializable {

    public SessionBean() {
    }
    
    public void checkSession() {
        try {
            FacesContext context = FacesContext.getCurrentInstance();
            Usuarios us = (Usuarios) context.getExternalContext().getSessionMap().get("Usuario");

            if (us == null) {
                context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Aviso!", "Debes de iniciar sesión para comenzar."));
                context.getExternalContext().redirect(context.getExternalContext().getRequestContextPath() + "/fail.xhtml");
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }
    }
    
    public Usuarios getUser() {
        return (Usuarios) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("Usuario");
    }
    
}
