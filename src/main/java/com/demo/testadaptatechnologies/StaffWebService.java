package com.demo.testadaptatechnologies;

import com.demo.entity.Address;
import com.demo.entity.Company;
import com.demo.entity.Geo;
import com.demo.entity.Usuarios;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author Gerardo Larín
 */
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author Gerardo Larín
 */
@ManagedBean(name = "staffWebService")
@ViewScoped
public class StaffWebService implements Serializable {

    private static String password = "root";

    private Usuarios us;
    private List<Usuarios> users;

    private String username;
    private String pswUsr;

    public StaffWebService() {
        super();
    }

    @PostConstruct
    public void init() {
        users = new ArrayList<Usuarios>();
    }

    public void loadData() throws IOException {
        Client c = Client.create();
        WebResource resource = c.resource("https://jsonplaceholder.typicode.com/users");
        String response = resource.get(String.class);

        JsonNode userNode = new ObjectMapper().readTree(response);

        List<Object> objetos = Arrays.asList(new ObjectMapper().readValue(response, Object.class));

        for (int i = 0; i < objetos.size(); i++) {
            List<Object> objs = (List<Object>) objetos.get(i);

            for (int j = 0; j < objs.size(); j++) {
                HashMap<String, Object> objs1 = (HashMap<String, Object>) objs.get(j);

                Usuarios usr = new Usuarios();
                Address address = new Address();
                Geo geo = new Geo();
                Company company = new Company();

                usr.setId((int) objs1.get("id"));
                usr.setName((String) objs1.get("name"));
                usr.setUsername((String) objs1.get("username"));
                usr.setEmail((String) objs1.get("email"));

                HashMap<String, Object> objs2 = (HashMap<String, Object>) objs1.get("address");
                address.setStreet((String) objs2.get("street"));
                address.setSuite((String) objs2.get("suite"));
                address.setCity((String) objs2.get("city"));
                address.setZipcode((String) objs2.get("zipcode"));

                HashMap<String, Object> objs3 = (HashMap<String, Object>) objs2.get("geo");
                geo.setLat((String) objs3.get("lat"));
                geo.setLng((String) objs3.get("lng"));

                address.setGeo(geo);

                usr.setAddress(address);
                usr.setPhone((String) objs1.get("phone"));
                usr.setWebsite((String) objs1.get("website"));

                HashMap<String, Object> objs4 = (HashMap<String, Object>) objs1.get("company");
                company.setName((String) objs4.get("name"));
                company.setCatchPhrase((String) objs4.get("catchPhrase"));
                company.setBs((String) objs4.get("bs"));

                usr.setCompany(company);

                users.add(usr);
            }
        }
    }

    public void log_in() {
        FacesContext context = FacesContext.getCurrentInstance();
        String redirect = null;
        try {
            loadData();
            for (int x = 0; x < users.size(); x++) {
                if (username.equals(users.get(x).getUsername())) {
                    if (pswUsr.equals(password)) {
                        context.getExternalContext().getSessionMap().put("Usuario", users.get(x));
                        redirect = "welcome.xhtml?faces-redirect=true";
                        FacesContext.getCurrentInstance().getExternalContext().redirect(redirect);
                    }
                }
            }
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", e.getMessage()));
            redirect = null;
        }
        context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error - ", "Usuario o contraseña no válidos"));
    }

    public void cerrarSesion() {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            context.getExternalContext().invalidateSession();
            context.getExternalContext().redirect(context.getExternalContext().getRequestContextPath() + "/index.xhtml");
        } catch (Exception e) {
            context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_FATAL, "Error", e.getMessage()));
        }
    }

    public Usuarios getUs() {
        return us;
    }

    public void setUs(Usuarios us) {
        this.us = us;
    }

    public List<Usuarios> getUsers() {
        return users;
    }

    public void setUsers(List<Usuarios> users) {
        this.users = users;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPswUsr() {
        return pswUsr;
    }

    public void setPswUsr(String pswUsr) {
        this.pswUsr = pswUsr;
    }
}
